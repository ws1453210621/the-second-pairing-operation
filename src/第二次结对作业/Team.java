package 第二次结对作业;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Arrays;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Team {
	static String[] webPages = new String[888];//创建一个足够大的来存放各个活动的网址的数组
	static int webNum = 0;// 记录数组真实长度
	static Student[] stuList = new Student[888];// 创建一个足够大的对象数组来存储姓名 学号 经验值
	static int stuNum = 0;// 真实数组长度
	static int createNum = 0;// 创建过程中变化的实际长度
	static int aver = 0;

	public static void main(String[] args) throws IOException {
		// 获取云班课经验地址和cookie
		String path= Team.class.getResource("config.properties").getPath();
		Properties config = new Properties();
		config.load(new FileInputStream(path));
		Document doc = Jsoup.connect(config.getProperty("url")).header("Cookie", config.getProperty("cookie"))
				.timeout(10000).get();
		// 获取总成员数
		String memNum = doc.getElementById("menu-content-box").select("a").get(1).select("span").get(1).text();
		stuNum = Integer.parseInt(memNum.substring(1, memNum.length() - 1));
		stuList =new Student[stuNum];
		// 获取课堂完成部分活动的网址
		Elements elements = doc.getElementsByClass("interaction-row");
		for (Element p : elements) {
			if (p.text().contains("课堂完成部分")) {
				getInfo(p, 1);
			}}
		// 获得各个人的学号 姓名 经验值 并存入集合
		for (int i = 0; i < webNum; i++) {
			Document student = Jsoup.connect(webPages[i]).header("Cookie", config.getProperty("cookie")).timeout(10000)
					.maxBodySize(0).get();
			Elements stuExp = student.getElementsByClass("homework-item");
			for (Element p : stuExp) {
				getInfo(p, 2);
			}}
		//实现1.排序 2.最高分 最低分 平均分 3.保存为txt文件
		Arrays.sort(stuList);
		File txt = new File("score.txt");
		PrintWriter out = new PrintWriter(new FileWriter(txt));
		String first = "最高经验值为:"+stuList[0].getExp()+"，"+"最低经验值为:"+stuList[stuNum-1].getExp()+"，"+"平均经验值为:"+aver/stuNum;
		out.print(first+"\n");
		for(Student stu:stuList)
			out.print(stu.toString()+"\n");
		out.close();
	}
	// 1、获取网址并存入数组
	// 2、获取名字 学号 经验值并存入对象集合
	public static void getInfo(Element p, int y) {
		if (y == 1) {
			String url = sort(p, "data-url=\"(\\S+)\"");
			webPages[webNum] = url.replace("&amp;", "&");
			webNum++;
		} else if (y == 2) {
			// 获取姓名 学号 经验值
			int exp = Integer.parseInt(sort(p, "(\\d+) 分</span>"));
			p = p.getElementsByClass("member-message").first();
			String name = sort(p, "color: #333;\">(\\S+)</span>");
			String idTxt = p.select("div").last().text();
			int id = 0;
			if(!idTxt.equals(""))
				id = Integer.parseInt(idTxt);
			// 将信息存入对象数组中
			int stuNo = -1;
			if(!name.equals("")){
				// 对象数组全为空的时候，先放入一个Student对象
				if (createNum == 0) {
					stuList[createNum] = new Student(name, id, exp);
					aver += exp;
					createNum++;
				} else {
					stuNo = find(id);// 查询其是否在对象数组中，并记录其下标的值，不存在于数组中为-1
					// 未在对象数组中的，将其加入数组中
					if (stuNo == -1) {
						stuList[createNum] = new Student(name, id, exp);
						aver += exp;
						createNum++;
					}
					// 在数组中的，则更新他的经验值
					else {
						stuList[stuNo].setExp(exp + stuList[stuNo].getExp());
						aver += exp;
					}
				}
            }
        }
    }
	// 通过正则表达式获取想要的信息
	public static String sort(Element p, String data) {
		String txt = p.toString();
		Pattern number = Pattern.compile(data);
		Matcher matcher = number.matcher(txt);
		String x = "";
		if (data.equals("(\\d+) 分</span>"))
			x = "0";
		while (matcher.find()) {
			x = matcher.group(1);
		}
		return x;
	}
	// 通过ID在对象数组查找其下标（在数组中的位置）
	public static int find(int id) {
		int IsExit = -1;
		for (int x = 0; x < createNum; x++) {
			if (stuList[x].getId()==id)
				IsExit = x;
		}
		return IsExit;
	}
}